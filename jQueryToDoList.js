/**
 * Toggles "done" class on <li> element
 */

$(document).ready(function(){
	
$(".today-list li").on("click", function(){
	const clickedElement = $(this);
	if(clickedElement.is( "li" )){
		clickedElement.toggleClass('active done');
	}
	else{
		clickedElement.parent().toggleClass('active done');
	}
});


/**
 * Delete element when delete link clicked
 */ 
// $(".today-list a").on("click", function(e){

$(".today-list a").on("click", function(){
	const clickedElement = $(this);
	 clickedElement.fadeOut(600,function(){
        clickedElement.parent().remove();
    })
	//.remove();
	//clickedElement.parent().remove();
});

/**
 * Adds new list item to <ul>
 */
 
const addListItem = function(e) {
	e.preventDefault();
    //const text = $('input').val();
    const $newLI = $('<li>');
	const $ul = $(".today-list")
    const $newSpan = $('<span>');
    const $newA = $('<a>');
	$newA.addClass('delete');
	$newA.text('Delete');
    $newSpan.text($('input').val());
	$newLI.append($newSpan);
	$newLI.append($newA);
	$ul.append($newLI);
	RefreshClickEvents();
	
  // rest here...
};

$(".add-item").on("click", function(e){
	addListItem(e);
	});
	
function RefreshClickEvents() {

	$(".today-list li").off();

	
    $(".today-list li").on("click", function(){
	const clickedElement = $(this);
	if(clickedElement.is( "li" )){
		clickedElement.toggleClass('active done');
	}
	else{
		clickedElement.parent().toggleClass('active done');
	}
});

$(".today-list a").off();

$(".today-list a").on("click", function(){
	const clickedElement = $(this);
	clickedElement.fadeOut(600,function(){
        clickedElement.parent().remove();
    })
});


}


  });